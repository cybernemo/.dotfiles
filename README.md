# dotfiles

This repo contains all dotfiles to configure my desktop environment.

[TOC]

## Prerequisites

Script to install the prerequisites tools to be configured with the install script.
For ArchLinux:

    curl -O -k https://gitlab.com/cybernemo/.dotfiles/-/raw/main/pre_archlinux
    bash pre_archlinux

For Ubuntu:

    curl -O -k https://gitlab.com/cybernemo/.dotfiles/-/raw/main/pre_ubuntu
    bash pre_ubuntu

## Install

Script to configure all tools installed with the prerequisite script.
For any distribution:

    curl -O -k https://gitlab.com/cybernemo/.dotfiles/-/raw/main/install
    bash install

## Tools

The following tools are installed and configured:

- NeoVim
- shell
- statusbar
- tmux
- wallpapers
- X11
- zsh

The following tools are also installed:

- bat
- lsd

### NeoVim

**Plugins**

- _vim-polyglot_: better syntax support
- _coc_: Conquer of Completion
- _nerdtree_: File explorer
- _nerdtree-git-plugin_: Git plugin for nerdtree
- _coc-json_: CoC json plugin
- _auto-pairs_: auto pairs for ( [ {
- _onedark_: dark theme
- _vim-devicons_: fonts
- _coc-prettier_: autoformater
- _vim-airline_: status/tabline
- _vim-puppet_: Puppet helper
- _neoterm_: terminal

### shell

### statusbar

In order to the icons to be render propoerly, *nerd-font.complete* needs to be installed.

### tmux

Open tmux:

```
tmux
```

Sessions manipulation:

```
F6 # detach session
```

Windows manipulation:

```
F2 # new window
F3 # next window
F4 # previous window
F5 # rename current window
F7 # kill current window
```

Screen manipulation:

```
Ctrl + v # split window vertically
Ctrl + h # split window horizontally
Ctrl + [h/j/k/l] # Vim-like screen navigation
```

### wallpapers

### X11

### zsh

## TODO
- [x] Update status bar item with buttons
- [ ] Add visual file manager
- [ ] Add clickable status bar icon
