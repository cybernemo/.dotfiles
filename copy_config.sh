#!/usr/bin/env bash
#
# This script installs the dotfiles

STOW_FOLDERS="nvim,shell,tmux,zsh,X11"
DOTFILES="$(pwd)"
STOW_FOLDER=unset

# Helper
usage()
{
  echo "Usage copy_config [ program-dotfile ] [ user ] "
  echo ""
  echo "Argument:"
  echo ""
  echo "program-dotfile     program from which to load the dotfiles"
  echo ""
  echo "user                user in which home directory to copy to"
  echo 
  exit 2
}

# Lister
list()
{
    echo "List of dotfile program"
    echo ""
    echo "STOW_FOLDERS" | sed "s/,/ /g"
}

STOW_FOLDER=$1

pushd $DOTFILES > /dev/null
stow -D $STOW_FOLDER
stow $STOW_FOLDER
popd > /dev/null

exit 0
