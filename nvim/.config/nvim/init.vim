" Load vim plugins
source $HOME/.config/nvim/vim-plug/plugins.vim

" Fundamentals settings
syntax on

" Looking settings
set number
set relativenumber
set cursorline
colorscheme onedark

" Mapping leader key to "Space"
map <SPACE> <Leader>

" coc-prettier
command! -nargs=0 Prettier :CocCommand prettier.formatFile

" vim-autoformatter
let g:python3_host_prog='/usr/bin/python3'

" NERDtree plugin
nnoremap <leader>nt :NERDTreeToggle<CR>
nnoremap <leader>nn :NERDTreeFocus<CR>

" Tabpage
nnoremap <leader>tc <cmd>:tabnew <cr> 
nnoremap <leader>tn <cmd>:+tabnext <cr>
nnoremap <leader>tp <cmd>:-tabnext <cr>
nnoremap <leader>tq <cmd>:tabclose <cr>

" Navigation between pane
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-l> :wincmd l<CR>

" Telescope
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

" Fugitive
nnoremap <leader>gd :Gdiffsplit<CR>
